var jimp = require('jimp'),
    promise = require('promise');

function Resizer() {};

Resizer.prototype.resizeImage = function (image, options) {
    return new promise((resolve, reject) => {
        if (options.w && options.h && options.method == 'contain') {
            image.contain(
                options.w, 
                options.h
            );
        }
        else if (options.w && options.h && options.method == 'cover') {
            image.cover(
                options.w, 
                options.h
            );
        }
        else if (options.w || options.h) {
            image.resize(
                options.w || jimp.AUTO, 
                options.h || jimp.AUTO
            );
        }

        image.write(options.filePath, resolve);
    });
};

Resizer.prototype.process = function (imageurl, options) {
    return new promise((resolve, reject) => {
        jimp.read(imageurl).then((image) => {
            this.resizeImage(image, options).then(() => {
                resolve();
            }, (err) => {
                reject();
            });
        }).catch((err) => {
            reject(err);
        });
    });
};

module.exports = Resizer;