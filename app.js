var express = require('express'),
    fs = require('fs'),
    resizer = new (require('./resizer')),
    exec = require('child_process').exec,
    md5 = require('md5'),
    app = express(),
    jimp = require('jimp'),
    promise = require('promise'),
    args = {};

(() => {
    process.argv.splice(2).forEach((arg) => {
        var argPieces = arg.split('=');
        args[argPieces[0]] = argPieces[1];
    });
})();

const TMP_PATH = args.path || "./tmp";
const BACKEND_URL = args.backend || "http://api.dev.ticketninja.io/cca-backend/api/v1/files";
const PORT = args.port || 3000;
const IMG_404 = args.img404 || "./img/404.png";

function sendBack(file, res, imghash, is304) {
    if (is304) {
        res.writeHead(304, {
            'ETag': imghash
        });
        res.end();
    }
    else {
        fs.readFile(file, (err, data) => {
            res.writeHead(200, {
                'Content-Type': 'image/png',
                'ETag': imghash
            });
            res.end(data);
        });
    }
};

function checkTmpPath() {
    if (!fs.existsSync(TMP_PATH)) {
        fs.mkdirSync(TMP_PATH);
    }
};

function get404Image(width, height) {
    return new promise((resolve, reject) => {
        var tmpData = {
                width: width || 320, 
                height: height || 240,
                scale: 1,
                bgcolor: 0xF4F4F4FF
            },
            img404filename = [TMP_PATH, md5(JSON.stringify(tmpData))].join("/") + ".png";

        if (!fs.existsSync(img404filename)) {
            jimp.read(IMG_404).then((img404) => {
                if (tmpData.width < 80 || tmpData.height < 80) {
                    tmpData.scale = Math.min(tmpData.width / 80, tmpData.height / 80);
                    img404.scale(tmpData.scale);
                }

                var image = new jimp(tmpData.width, tmpData.height, tmpData.bgcolor, function (err, image) {
                    image
                        .composite(img404, tmpData.width / 2 - 160 * tmpData.scale, tmpData.height / 2 - 120 * tmpData.scale)
                        .write(img404filename, () => {
                            resolve(img404filename);
                        });
                });
            });
        }
        else {
            exec('touch ' + img404filename);
            resolve(img404filename);
        }
    });
};

app.get('/api/v1/images/:imghash', (req, res) => {
    var fileName = [req.params.imghash, req.query.hash, md5(JSON.stringify(req.query))].join("_"),
        filePath = [TMP_PATH, fileName].join("/") + ".png",
        imageurl = [BACKEND_URL, req.params.imghash].join("/");

    checkTmpPath();

    if (!req.query.hash || 
        (!isNaN(+req.query.width) && (+req.query.width < 1 || +req.query.width > 5000)) || 
        (!isNaN(+req.query.height) && (+req.query.height < 1 || +req.query.height > 5000))) {
        res.writeHead(400, {'Content-Type': 'image/png'});
        res.end();
    }
    else {
        if (!fs.existsSync(filePath)) {
            resizer.process(imageurl, {
                w: +req.query.width || 0,
                h: +req.query.height || 0,
                method: req.query.method || "",
                filePath: filePath
            }).then(() => {
                sendBack(
                    filePath, 
                    res, 
                    req.params.imghash
                );
            }, (err) => {
                if (!isNaN(+req.query.width) || !isNaN(+req.query.height)) {
                    get404Image(+req.query.width, +req.query.height).then((img404) => {
                        sendBack(
                            img404, res, 
                            req.params.
                            imghash
                        );
                    });
                }
                else {
                    res.writeHead(400, {'Content-Type': 'image/png'});
                    res.end();
                }

            });
        }
        else {
            exec('touch ' + filePath);
            sendBack(
                filePath, 
                res, 
                req.params.imghash, 
                req.headers['if-none-match'] == req.params.imghash
            );
        }
    }
});

app.listen(PORT, () => {
    console.log('Resizer listening on port ' + PORT + '!')
});0